#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example implementation of splitting a line a text into a
# list of words by separating on each comma.
# NOTE: The string.split(',') method does the same thing.

row = 'Homer Simpson,10.0,50.0,30.0,45.0'  # Row to split
columns = []  # The list to put the columns into

# Loop through the row by repeatedly finding the next comma index
next_comma_index = row.find(',')
while next_comma_index >= 0:
    # Pull out the next column and append it to the list
    column = row[:next_comma_index]
    columns.append(column)

    # Focus on the remainder of the row (the +1 skips the comma)
    row = row[next_comma_index+1:]
    next_comma_index = row.find(',')

# Append the remain part of the row as the final column
columns.append(row)

print(columns)
