#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example of optimising code. The goal of this program is to find all
# actors who appeared in at least one movie with Tom Cruise. We will
# begin by loading a file of (actor, movie) pairs. We will then find
# all movies in which Tom Cruise appears. Then we will find all actors
# who have appeared in each of those movies and return them as a
# sorted list.
#

import cProfile
import csv

def read_data(filename):
    """Read in dataset as list of (actor, movie) pairs and return
    as a dictionary of movie casts indexed by movie title."""
    fh = open(filename, "r")
    reader = csv.reader(fh, delimiter=";")

    dataset = {}
    nCount = 0
    for actor, movie in reader:
        movie = movie.strip()
        if movie not in dataset:
            dataset[movie] = set()
        dataset[movie].add(actor.strip())
        nCount += 1

    fh.close()
    print("...read {} actor-movie pairs".format(nCount))
    return dataset

def find_movies(star, dataset):
    """Find all movies starring the given actor."""
    acted_in = []
    for movie, cast in dataset.items():
        if star in cast:
            acted_in.append(movie)

    return acted_in

def find_actors(title, dataset):
    """Find all actors starring in the given movie."""
    return dataset[title]

def find_costars(star, dataset):
    """Finds all costars who have appeared with the given actor in at least one movie
    and returns in sorted order."""

    # first find movies containing the star
    movies = find_movies(star, dataset)

    # initialise set of costars
    costars = set()

    # for each movie search for the costars
    for title in movies:
        costars |= set(find_actors(title, dataset))

    # remove star, sort, and return costars
    costars.remove(star)
    return sorted(list(costars))


# search costars of Tom Cruise
def main():
    dataset = read_data("actor-movie.csv")
    for i in range(1000):
        costars = find_costars("Cruise, Tom", dataset)

# do profiling
if __name__ == "__main__":
    cProfile.run("main()", sort="tottime")
