#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example of optimising code. The goal of this program is to find all
# actors who appeared in at least one movie with Tom Cruise. We will
# begin by loading a file of (actor, movie) pairs. We will then find
# all movies in which Tom Cruise appears. Then we will find all actors
# who have appeared in each of those movies and return them as a
# sorted list.
#

import cProfile
import csv

def read_data(filename):
    """Read in dataset as list of (actor, movie) pairs."""
    fh = open(filename, "r")
    reader = csv.reader(fh, delimiter=";")

    dataset = []
    for actor, movie in reader:
        dataset.append((actor.strip(), movie.strip()))

    fh.close()
    print("...read {} actor-movie pairs".format(len(dataset)))
    return dataset

def find_movies(star, dataset):
    """Find all movies starring the given actor."""
    acted_in = []
    for actor, movie in dataset:
        if actor == star:
            acted_in.append(movie)

    return acted_in

def find_actors(title, dataset):
    """Find all actors starring in the given movie."""
    cast = []
    for actor, movie in dataset:
        if movie == title:
            cast.append(actor)

    return cast

def find_costars(star, dataset):
    """Finds all costars who have appeared with the given actor in at least one movie
    and returns in sorted order."""

    # first find movies containing the star
    movies = find_movies(star, dataset)
    print("{} appears in {}".format(star, ", ".join(movies)))

    # initialise list of costars
    costars = []

    # for each movie search for the costars
    for title in movies:
        cast = find_actors(title, dataset)
        for actor in cast:
            if actor != star:
                print("{} appears with {} in {}".format(actor, star, title))
                if actor not in costars:
                    # insert into list and maintain sorted order
                    costars.append(actor)
                    costars.sort()

    # return costars
    return costars

# search costars of Tom Cruise
def main():
    dataset = read_data("actor-movie.csv")
    for i in range(1000):
        costars = find_costars("Cruise, Tom", dataset)

# do profiling
if __name__ == "__main__":
    cProfile.run("main()", sort="tottime")
