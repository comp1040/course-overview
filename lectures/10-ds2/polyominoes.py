#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# A polyomino is a geometric shape composed of joining one or more
# squares, connected orthogonally. For example, the popular Tetris
# game involve fitting falling tetrominoes (shapes composed of four
# squares). The following code demonstrates how to perform a search
# for fitting a pre-defined set of polyominoes into a fixed grid,
# in our case six polyominoes into a 5-by-5 grid. The efficiency of
# the program can be improved by making various changes but the
# basic structure works as is.

from copy import deepcopy

# --- Problem Definition -------------------------------------------

"""
A problem is defined by the number of rows and columns of the grid and
a list of pieces. We define a piece by an n-tuple of points relative
to some reference point, which we take to be (0, 0). The pieces can be
rotated in 90 degree increments. For efficiency we encode the number of
rotations that result in unique configurations for each piece. We do
not allow reflections.
"""

"""

ROWS = 5
COLS = 5

PIECES = [
    ((0, 0), (0, 1), (0, 2), (0, 3)),         # straight
    ((0, 0), (0, 1), (1, 1), (1, 0)),         # square
    ((0, 0), (0, 1), (0, 2), (1, 1)),         # 4-tee
    ((0, 0), (0, 1), (0, 2), (0, 3), (1, 2)), # 5-tee
    ((0, 0), (0, 1), (0, 2), (1, 2)),         # ell
    ((0, 0), (0, 1), (1, 1), (1, 2))          # zed
]

ROTATIONS = [2, 1, 4, 4, 4, 2]

"""

ROWS = 5
COLS = 8

PIECES = [
    ((0, 0), (0, 1), (0, 2), (0, 3)),         # straight
    ((0, 0), (0, 1), (1, 1), (1, 0)),         # square
    ((0, 0), (0, 1), (0, 2), (1, 1)),         # tee
    ((0, 0), (0, 1), (0, 2), (1, 2)),         # ell
    ((0, 0), (0, 1), (-1, 1), (-1, 2)),       # ess
    ((0, 0), (0, 1), (0, 2), (0, 3)),         # straight
    ((0, 0), (0, 1), (1, 1), (1, 0)),         # square
    ((0, 0), (0, 1), (0, 2), (1, 1)),         # tee
    ((0, 0), (0, 1), (0, 2), (-1, 2)),        # jay
    ((0, 0), (0, 1), (1, 1), (1, 2))          # zed
]

ROTATIONS = [2, 1, 4, 4, 2, 2, 1, 4, 4, 2]

# --- Board and Piece Manipulation ---------------------------------

def rotate(piece):
    """Rotate a piece by 90 degrees anti-clockwise."""
    rotated_piece = [(p[1], -p[0]) for p in list(piece)]
    return tuple(rotated_piece)

def place_piece(board, ref_point, piece, tag):
    """Add a new piece to the board at the given reference point and
    return the new board. Returns None if the piece could not be added
    at the given location."""

    # Check that reference location is free.
    if (board[ref_point[0]][ref_point[1]] is not None):
        return None

    # We need to make a deep copy of the board, otherwise we will
    # be modifying the input.
    new_board = deepcopy(board)
    for p in piece:
        x = ref_point[0] + p[0]
        y = ref_point[1] + p[1]
        if (0 <= x < len(board)) and (0 <= y < len(board[0])) and (board[x][y] is None):
            new_board[x][y] = tag
        else:
            return None

    return new_board

def remove_piece(board, tag):
    """Removes a piece from the board."""

    # We need to make a deep copy of the board, otherwise we will
    # be modifying the input.
    new_board = deepcopy(board)
    for x in range(len(board)):
        for y in range(len(board[0])):
            if (board[x][y] == tag):
                new_board[x][y] = None

    return new_board

def display_board(board):
    """Displays the given board configuration."""

    for x in range(len(board)):
        print(" ".join(str(board[x])))

# --- Search Routine -----------------------------------------------

def add_piece_to_search(frontier, board, tag):
    """Expands a node by adding all possible moves for a piece with
    the given tag to the frontier."""

    for x in range(COLS):
        for y in range(ROWS):
            p = PIECES[tag]
            for rotations in range(ROTATIONS[tag]):
                new_board = place_piece(board, (x, y), p, tag)
                if (new_board is not None):
                    frontier.append([new_board, tag + 1])
                p = rotate(p)

# --- Main ---------------------------------------------------------

if __name__ == "__main__":

    """
    We perform depth-first search (DFS) by maintaining a frontier of
    nodes already expanded. Nodes are popped from the stack, expanded
    and the new nodes pushed onto the stack. A node consists of the
    board state and the next piece to add.
    """

    # initialise stack of expanded nodes
    frontier = []

    # try first piece (expand root node) on empty board
    board = [[None for y in range(ROWS)] for x in range(COLS)]
    add_piece_to_search(frontier, board, 0)

    # iteratively remove nodes from the stack and expand them
    nodes_popped = 0
    while (len(frontier) > 0):
        board, tag = frontier.pop()
        nodes_popped += 1

        # check if we've added all the pieces
        if (tag >= len(PIECES)):
            display_board(board)
            exit()

        if (nodes_popped % 10 == 0): print("..." + str(nodes_popped), end="\r")
        add_piece_to_search(frontier, board, tag)

    # we have not been able to solve the puzzle
    print("Puzzle is impossible")
