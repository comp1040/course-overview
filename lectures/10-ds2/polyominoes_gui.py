#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# GUI wrapper for polyominoes.py 
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.patches import RegularPolygon
import polyominoes as p

def animate(fnum):
    """Animation function for expanding the search frontier."""

    # use global frontier
    global nodes_expanded, frontier, squares, ax

    # stop if we've exhuasted the frontier
    if (len(frontier) == 0):
        return None

    # pre-defined colours
    COLOURS = ["green", "blue", "yellow", "red", "cyan", "purple",
               "gray", "orange", "limegreen", "pink"]

    # get the current board state
    board, tag = frontier.pop()
    nodes_expanded += 1
    ax.set_title("{} nodes expanded".format(nodes_expanded))

    # draw the board
    for row in range(p.ROWS):
        for col in range(p.COLS):
            if (board[col][row] is not None):
                squares[col, row].set_facecolor(COLOURS[board[col][row]])
            else:
                squares[col, row].set_facecolor("#ffffff")

    # expand frontier if there are still pieces to add
    if (tag < len(p.PIECES)):
        p.add_piece_to_search(frontier, board, tag)
    else:
        # otherwise empty the frontier (we've found a solution)
        frontier.clear()

# initialise search
frontier = []

# try first piece (expand root node) on empty board
nodes_expanded = 0
board = [[None for y in range(p.ROWS)] for x in range(p.COLS)]
p.add_piece_to_search(frontier, board, 0)

# initialise graphics and start animation
plt.ioff()                # turn off interactive mode
fig = plt.figure()        # intialize the figure
ax = fig.add_axes((0.05, 0.05, 0.9, 0.9), aspect="equal", frameon=False,
                  xlim=(-0.05, p.COLS + 0.05), ylim=(-0.05, p.ROWS + 0.05))
ax.xaxis.set_major_formatter(plt.NullFormatter())
ax.yaxis.set_major_formatter(plt.NullFormatter())
ax.xaxis.set_major_locator(plt.NullLocator())
ax.yaxis.set_major_locator(plt.NullLocator())
squares = np.array([[RegularPolygon((i + 0.5, j + 0.5), numVertices=4, radius=0.5 * np.sqrt(2),
                                    orientation=np.pi / 4, ec="#000000", fc="#ffffff")
                     for j in range(p.ROWS)] for i in range(p.COLS)])
[ax.add_patch(sq) for sq in squares.flat]
ani = animation.FuncAnimation(fig, animate, interval=10, repeat=False)
plt.show()
