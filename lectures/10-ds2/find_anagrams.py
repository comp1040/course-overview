#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Find sets of anagrams in text from a given file. The implementation
# assumes that the entire file can fit into memory.
#

import string
import sys

# -----------------------------------------------------------------

def anagram_signature(text):
    """Converts a string into a tuple of letter counts."""
    lower_case = ''.join(filter(str.isalpha, text)).lower()
    return tuple([lower_case.count(i) for i in string.ascii_lowercase])

# -----------------------------------------------------------------

# open file and hash signatures into dictionary
filename = "anagrams.txt"
anagram_sets = dict()
for line in open(filename):
    line = line.rstrip('\n')        # remove line ending
    sig = anagram_signature(line)   # create signature
    if sig not in anagram_sets:     # if new signature add to dictionary
        anagram_sets[sig] = set()
    anagram_sets[sig].add(line)     # add to set of lines with same signature

# find and print all anagram sets of size greater than one
for sig in anagram_sets.keys():
    if len(anagram_sets[sig]) > 1:
        print("-" * 80)
        for line in anagram_sets[sig]:
            print(line)

