#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Even more code snippets from Lecture 4

import math
theta = 2.0 * math.pi * 45.0 / 360.0    # 45 degrees in radians
point = (math.cos(theta), math.sin(theta))
print(point)

t = tuple("COMP1040")
print(t)

names = ["Bart", "List"]   # construct a list with two elements
names.append("Maggie")     # add a third element
print(names)               # print the list

perfect_squares = [(x + 1) ** 2 for x in range(10)]
print(perfect_squares)

fullnames = [n + " Simpson" for n in names]
print(fullnames)
