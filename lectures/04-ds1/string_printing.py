#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# More code snippets from Lecture 4

string_A = "Hello World"  # valid string
string_B = 'Hello World'  # valid string, same as string_A

print("The Craft of Computing")
print(1040)

import math
print("The number pi is {:.2f}".format(math.pi))

print("{} and {}".format("Bart", "Lisa"))
print("{0} and {1}".format("Bart", "Lisa"))
print("{1} and {0}".format("Bart", "Lisa"))

for i in range(100):
    print("{:4d}".format(i))

message = "Hello World"
for i in range(len(message)):
    print(message[i])
