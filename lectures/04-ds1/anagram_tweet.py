#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Determine if two tweets (of at most 140 characters) are anagrams.
#

# -----------------------------------------------------------------
# Version 1: Remove non-alphabetic, convert to lowercase, sort
# and compare.
# -----------------------------------------------------------------

import string

tweet_A = "Everything is always too good to be true!"
tweet_B = "I guess I have to try to go to bed early now."

signature_A = sorted(''.join(filter(str.isalpha, tweet_A)).lower())
signature_B = sorted(''.join(filter(str.isalpha, tweet_B)).lower())

if (signature_A == signature_B):
    print("The tweets are anagrams")
else:
    print("The tweets are not anagrams")

# -----------------------------------------------------------------
# Version 2: Create histogram of 26 characters and compare.
# -----------------------------------------------------------------

lower_case_A = ''.join(filter(str.isalpha, tweet_A)).lower()
signature_A = [lower_case_A.count(i) for i in string.ascii_lowercase]

lower_case_B = ''.join(filter(str.isalpha, tweet_B)).lower()
signature_B = [lower_case_B.count(i) for i in string.ascii_lowercase]

if (signature_A == signature_B):
    print("The tweets are anagrams")
else:
    print("The tweets are not anagrams")

