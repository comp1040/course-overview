#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Calculate the area and circumference of a circle with radius from
# user input.

import math

radius = float(input("Enter radius: "))
area = math.pi * radius ** 2
circumference = 2 * math.pi * radius
print (area, circumference)
