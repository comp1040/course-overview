"""
Summary functions.
"""
__author__ = 'mreid'

def sum_columns(table):
    """
    Sum the columns of the given table.
    :param table: A list of list of numbers
    :return: A list with the sums of each columns of table
    """
    if not table: return None

    num_cols = len(table[0])
    sums = [0] * num_cols

    for row in table:
        for i, value in enumerate(row):
            sums[i] += value

    return sums

def sum_rows(table):
    """
    Sum the rows of the given table.
    :param table: A list of lists of numbers.
    :return: A list with the sums of the table's rows
    """
    if not table: return None

    sums = []
    for row in table:
        sums.append(sum(row))

    return sums

