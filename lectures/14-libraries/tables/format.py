"""
Formatting module for tables.
"""
__author__ = 'mreid'

def format_table(table):
    """
    Convert a table into an aligned, formatted string.
    :param table: A list of list of numbers
    :return: A string representation of the table
    """
    result = ""
    for row in table:
        row = [str(entry) for entry in row]
        result += '\t'.join(row) + '\n'

    return result

