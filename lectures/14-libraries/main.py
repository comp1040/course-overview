"""
COMP1040 Craft of Computing

A simple script for summarising and displaying tables of
numbers.

AUTHOR: Mark Reid <mark.reid@anu.edu.au>
"""
#-------------------------------------------------------------------------------
# Try out the above code on an example.
import tables.format as tblfmt
import tables.summary

table = [
    [0,0,0,0,0],
    [1,2,3,4,5],
    [2,4,6,8,10],
    [3,6,9,12,15]
]
print("The table looks like this:")
print(tblfmt.format_table(table))
print("The sums of its columns are:")
print(tables.summary.sum_columns(table))
