# COMP1040 - Craft of Computing
# Lecture 24: Refactoring
#
# Final version of the code in `refactor.py`.

import csv

RED_CSV = 'winequality-red.csv'
WHITE_CSV = 'winequality-white.csv'

def mean(xs):
    return sum(xs) / len(xs)

def read_attribute(filename, attribute):
    data = []
    with open(filename) as datafile:
        for row in csv.DictReader(datafile, delimiter=';'):
            data.append(float(row[attribute]))
    return data

# Get results for red and white wine
for filename in [RED_CSV, WHITE_CSV]:
    print('Results for {}:'.format(filename))

    for attr in ['pH', 'alcohol', 'quality']:
        print('\t{}:'.format(attr))
        data = read_attribute(filename, attr)
        for stat in [mean, max, min]:
            print('\t\t{}: {:.2f}'.format(stat.__name__, stat(data)))
