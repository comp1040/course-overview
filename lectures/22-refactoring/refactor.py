# COMP1040 - Craft of Computing
# Lecture 24: Refactoring
#
# Code to compute some statistics from the wine quality data sets.
#
# NOTE: This is deliberately poorly written so as to demonstrate
#       why refactoring is important

import csv

# Get the means for red wine
print('Red wine:')

print('\tpH:')
# Calculate the average pH content in the red wine data set
with open('winequality-red.csv') as datafile:
    sum = 0
    count = 0
    for row in csv.DictReader(datafile, delimiter=';'):
        sum += float(row['pH'])
        count += 1
print('\t\tmean: {:.2f}'.format(sum / count))

print('\talcohol:')
# Calculate the average alcohol content in the red wine data set
with open('winequality-red.csv') as datafile:
    sum = 0
    count = 0
    for row in csv.DictReader(datafile, delimiter=';'):
        sum += float(row['alcohol'])
        count += 1
print('\t\tmean: {:.2f}'.format(sum / count))

# Do the same for white wine
print('White wine:')

print('\tpH:')
# Calculate the average pH content in the white wine data set
with open('winequality-white.csv') as datafile:
    sum = 0
    count = 0
    for row in csv.DictReader(datafile, delimiter=';'):
        sum += float(row['pH'])
        count += 1
print('\t\tmean: {:.2f}'.format(sum / count))

print('\talcohol:')
# Calculate the average alcohol content in the white wine data set
with open('winequality-white.csv') as datafile:
    sum = 0
    count = 0
    for row in csv.DictReader(datafile, delimiter=';'):
        sum += float(row['alcohol'])
        count += 1
print('\t\tmean: {:.2f}'.format(sum / count))

