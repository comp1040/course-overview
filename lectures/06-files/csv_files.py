#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Demonstrate the reading and writing of CSV files.

import csv

# initialise list of final grades
final_grades = []

# read student file and compute final grades
fh = open("student_grades.csv", "r")
reader = csv.reader(fh)
next(reader) # skip the first row
for row in reader:
    student_name = row[0]
    student_grade = 0.25 * sum([float(i) for i in row[1:]])
    final_grades.append([student_name, student_grade])

fh.close()

# write grades to a new file
fh = open("final_grades.cvs", "w", newline="")
writer = csv.writer(fh)
writer.writerow(["Name", "Final Grade"])
for row in final_grades:
    writer.writerow(row)

fh.close()
