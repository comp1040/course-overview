#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#

with open("rscs_academics.txt", "r") as fin:
    with open("formatted_names.txt", "w") as fout:
        for name in fin:
            p = name.find(",")
            if (p == -1): continue
            lastname = name[0:p].strip().title()
            firstname = name[p+1:].strip()
            fout.write("{}. {}\n".format(firstname[0], lastname))
