#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#

import glob
import os

# open HTML document and print header information
fh = open("my_report.html", "w")
fh.write("<html>\n<head><title>My Results</title>\n</head>\n")
fh.write("<body>\n")

# Read all .jpg files from one directory and assume that a
# file with the same name appears in the second directory.
# Display the images side-by-side in a table.
fh.write("<table>\n")
fh.write("<tr><th>name</th><th>dir_1</th><th>dir_2</th></tr>\n")
for imgfile in glob.glob("dir_1/*.jpg"):
    basename = os.path.basename(imgfile)
    fh.write("<tr>")
    fh.write("<td>" + basename + "</td>")
    fh.write("<td><img src='dir_1/" + basename + "'></td>")
    fh.write("<td><img src='dir_2/" + basename + "'></td>")
    fh.write("</tr>\n")

fh.write("</table>\n")

# print footer information and close HTML document
fh.write("</body>\n</html>\n")
fh.close()
