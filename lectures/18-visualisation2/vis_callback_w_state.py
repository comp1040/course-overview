#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example animation using callbacks with state
#

import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def animate(fnum, x, theta):
    """Animation function for plotting a phase-shifted sine wave."""
    y = np.sin(x + theta[0])    # compute the shifted sine wave
    theta[0] += 0.02 * math.pi  # update state

    plt.cla()               # clear the plot on the current axes
    plt.plot(x, y, 'b-')    # plot as a line graph
    plt.xlim(x[0], x[-1])   # set the range of the x-axis
    plt.title("$\\theta = {:0.3f}$".format(theta[0]))

    return plt.gca()        # update the entire axes

# main function
f = plt.figure()            # intialize the figure
plt.ioff()                  # turn off interactive mode

# initialise state information
x = np.linspace(0.0, 4.0 * math.pi, 100)
theta = [0.0]               # note: list

# register the callback function and show
ani = animation.FuncAnimation(f, animate, fargs=(x, theta),
    interval=100, frames=100, repeat=False)
plt.show()
