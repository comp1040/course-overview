#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example animation using draw and sleep
#

import math
import numpy as np
import matplotlib.pyplot as plt

plt.figure()                # intialize the figure
plt.ion()                   # turn on interactive mode
plt.show()                  # show figure window

for theta in np.linspace(0.0, 2.0 * math.pi, 100):
    x = np.linspace(0.0, 4.0 * math.pi, 100)
    y = np.sin(x + theta)   # compute the shifted sine wave
    plt.cla()               # clear the plot on the current axes
    plt.plot(x, y, 'b-')    # plot as a line graph
    plt.xlim(x[0], x[-1])   # set the range of the x-axis
    plt.title("$\\theta = {:0.3f}$".format(theta))
    plt.draw()              # tell Python to update the plot

    plt.pause(0.1)         # delay for 100ms before the next plot
