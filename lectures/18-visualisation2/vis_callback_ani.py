#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example animation using callbacks
#

import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

def animate(fnum):
    """Animation function for plotting a phase-shifted sine wave."""
    theta = 2.0 * math.pi * float(fnum) / 100.0
    x = np.linspace(0.0, 4.0 * math.pi, 100)
    y = np.sin(x + theta)   # compute the shifted sine wave

    plt.cla()               # clear the plot on the current axes
    plt.plot(x, y, 'b-')    # plot as a line graph
    plt.xlim(x[0], x[-1])   # set the range of the x-axis
    plt.title("$\\theta = {:0.3f}$".format(theta))

    return plt.gca()        # update the entire axes

# main function
f = plt.figure()            # intialize the figure
plt.ioff()                  # turn off interactive mode

# register the callback function and show
ani = animation.FuncAnimation(f, animate, interval=100, frames=100, repeat=False)
plt.show()
