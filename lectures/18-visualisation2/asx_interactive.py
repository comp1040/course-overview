#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example extraction of data from the ASX and plotting.
#

import urllib.request
from html.parser import HTMLParser
import re
import os

# query asx for volumes traded per month over the last year
URL = "http://www.asx.com.au/asx/statistics/tradingVolumes.do"

# grab data if the file "asx.html" does not exist locally
if not os.path.exists("asx.html"):
    print("Reading ASX data from website...")
    response = urllib.request.urlopen(URL)
    html = str(response.read())
    response.close()

    html = re.sub("\\\\t|\\\\r\\\\n", "", html)

    # save to file
    with open("asx.html", "wt") as f:
        f.write(html)

else:
    # load from file
    print("Reading ASX data from cached file...")
    with open("asx.html", "rt") as f:
        html = f.readline()

class ASXHTMLParser(HTMLParser):
    """Parser to extract monthly trading table from ASX html page."""

    # member variables
    tableCount = 0
    lineCount = 0
    insideTable = False
    tradingTable = list()

    def handle_starttag(self, tag, attrs):
        if (tag == "table"):
            self.tableCount += 1
            self.lineCount = 0
            if (self.tableCount == 2):
                self.insideTable = True
        if (tag == "tr") and self.insideTable and (self.lineCount > 0):
            self.tradingTable.append(list())

    def handle_data(self, data):
        if self.insideTable and (self.lineCount > 0):
            self.tradingTable[-1].append(data.strip())

    def handle_endtag(self, tag):
        if (tag == "table"):
            self.insideTable = False
        if (tag == "tr") and self.insideTable:
            self.lineCount += 1

# parse HTML to extract table contents
parser = ASXHTMLParser()
parser.feed(html)

# plot equity trades by month as bar graph
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

volume = []
months = []

for row in reversed(parser.tradingTable):
    months.append(row[0].replace(" ", "\n"))
    volume.append(float(row[1].replace(",", "")))

index = np.arange(len(volume));
cmap = matplotlib.cm.get_cmap('Spectral')
cnorm = matplotlib.colors.Normalize(0, len(index) - 1)
colours = cmap(cnorm(index))

def plot_bar(volume, months, colours):
    """Plot ASX monthly volume data as a bar chart."""
    width = 0.65
    index = np.arange(len(volume));

    plt.clf()
    plt.bar(index, volume, width, color=colours)
    plt.ylabel("Volume")
    plt.title("Previous Year's Equity Trades")
    plt.xticks(index + 0.5 * width, months)

def plot_month_pie(data_row, month_label):
    """Plot pie chart for a given month"""
    row_data = [float(value.replace(",", "")) for value in data_row]

    plt.clf()
    plt.pie(row_data, labels=['Equity', 'Interest\nRates', 'ETOs', 'Warrants'])
    plt.title("Volume for " + month_label.replace("\n", ", "))

# show bar chart and allow users to investigate individual months
while True:
    plot_bar(volume, months, colours)                       # draw a bar chart for monthly volume
    plt.draw()                                              # update the figure

    pt = plt.ginput(1)                                      # wait for a mouse click (no need for plt.show())
    month_indx = int(pt[0][0])                              # determine the month that was clicked
    if 0 <= month_indx < len(months):
        row = parser.tradingTable[-1 - month_indx][1:-1]    # extract row in table corresponding to the month
        plot_month_pie(row, months[month_indx])             # plot a pie chart for that month
        plt.draw()                                          # update the figure
        plt.waitforbuttonpress()                            # wait for keyboard or mouse click
