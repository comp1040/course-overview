#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example of multi-dimensional plotting for Lecture 21: Visualisation II.
#

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# load the wine quality dataset and display attributes
wine = pd.read_csv('winequality-red.csv', sep=';')
for attribute in wine:
    print(attribute)

# print average 'pH' across the entire dataset
print("mean pH is {}".format(np.mean(wine['pH'])))

# 2d plot
wine.plot(kind='scatter', x='pH', y='alcohol')
plt.show()

# 3d plot
from mpl_toolkits.mplot3d import Axes3D

ax = plt.figure().gca(projection='3d')
ax.scatter(wine['pH'], wine['alcohol'], wine['quality'])
ax.set_xlabel('pH')
ax.set_ylabel('alcohol')
ax.set_zlabel('quality')
plt.show()

# multiple plots
pd.tools.plotting.scatter_matrix(wine)
plt.show()

