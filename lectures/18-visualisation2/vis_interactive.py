#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Simple user interaction with matplotlib.
#

import matplotlib.pyplot as plt
import matplotlib

# waiting for a key press
plt.figure()
for n in range(1, 10):
    plt.cla(); plt.axis('off')
    plt.gca().text(0.5, 0.5, "Stage {}\npress a key to continue".format(n),
        horizontalalignment='center', fontsize=24)
    plt.draw()
    plt.waitforbuttonpress()

# mouse input
plt.figure()
plt.cla()
points = plt.ginput(5)
for i, (x, y) in enumerate(points):
    plt.plot(x, y, 'bo')
    plt.gca().text(x, y, "Point {}".format(i + 1))
plt.draw()
plt.show()
