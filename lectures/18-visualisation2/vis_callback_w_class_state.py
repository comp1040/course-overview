#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example animation using callbacks with state implemented as a class
#

import math, time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

class SineWaveAnimationState:
    """Holds state information for animating a sine wave."""
    def __init__(self):
        self.x = np.linspace(0.0, 4.0 * math.pi, 100)
        self.theta = 0.0

    def y(self):
        """Returns the shifted sine wave."""
        return np.sin(self.x + self.theta)

    def inc_theta(self, amount=0.02):
        """Increment theta."""
        self.theta += amount

def animate(fnum, state):
    """Animation function for plotting a phase-shifted sine wave."""
    plt.cla()               # clear the plot on the current axes
    plt.plot(state.x, state.y(), 'b-')    # plot as a line graph
    plt.xlim(state.x[0], state.x[-1])     # set the range of the x-axis
    plt.title("$\\theta = {:0.3f}$".format(state.theta))
    state.inc_theta()       # increment theta

    return plt.gca()        # update the entire axes

# main function
f = plt.figure()            # intialize the figure
plt.ioff()                  # turn off interactive mode

# initialise state information
state = SineWaveAnimationState()

# register the callback function and show
ani = animation.FuncAnimation(f, animate, fargs=(state,),
    interval=100, frames=100, repeat=True)

plt.show()
