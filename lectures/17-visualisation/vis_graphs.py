#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Graph visualisation using networkx

import networkx as nx
import matplotlib.pyplot as plt

# create a graph
g = nx.Graph()

# add some nodes
names = ["Steve", "Mark", "Felicity", "Sam"]
for n in names:
    g.add_node(n)

# add some edges
g.add_edge("Steve", "Felicity")
g.add_edge("Steve", "Sam")
g.add_edge("Mark", "Felicity")
g.add_edge("Mark", "Sam")

# visualise
nx.draw(g, with_labels=True, font_size=24)
plt.show()

# fancier visualisation
positions = nx.spring_layout(g)
nx.draw_networkx_edges(g, positions, edge_color="#7f0000", width=5)
nx.draw_networkx_nodes(g, positions, nodelist=["Steve", "Mark"], node_color="#007f00", node_shape="s")
nx.draw_networkx_nodes(g, positions, nodelist=["Felicity", "Sam"], node_color="#00007f", node_shape="o")
plt.axis('off')
plt.show()