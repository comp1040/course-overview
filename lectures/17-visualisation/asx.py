#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example extraction of data from the ASX and plotting. The code
# will cache data from the ASX website in a local file called
# "asx.html". Delete that file if you want to refresh the data.
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

def plot_bar(volume, months, colours):
    """Plot ASX montly volume data as a bar chart."""
    width = 0.65
    index = np.arange(len(volume));

    plt.bar(index, volume, width, color=colours)

    plt.ylabel("Volume")
    plt.title("Previous Year's Equity Trades")
    plt.xticks(index + 0.5 * width, months)
    plt.show()

def plot_pie(volume, months, colours):
    """Plot ASX montly volume data as a pie chart."""

    explode = [0 for i in range(len(volume))]
    explode[-1] = 0.1
    plt.pie(volume, labels=months, explode=explode, colors=colours)
    plt.title("Previous Year's Equity Trades")
    plt.show()


# --- download and parse asx data --------------------------------

import urllib.request
from html.parser import HTMLParser
import re
import os

# query asx for volumes traded per month over the last year
URL = "http://www.asx.com.au/asx/statistics/tradingVolumes.do"

# grab data if the file "asx.html" does not exist locally
if not os.path.exists("asx.html"):
    print("Reading ASX data from website...")
    response = urllib.request.urlopen(URL)
    html = str(response.read())
    response.close()

    html = re.sub("\\\\t|\\\\r\\\\n", "", html)

    # save to file
    with open("asx.html", "wt") as f:
        f.write(html)

else:
    # load from file
    print("Reading ASX data from cached file...")
    with open("asx.html", "rt") as f:
        html = f.readline()

class ASXHTMLParser(HTMLParser):
    """Parser to extract monthly trading table from ASX html page."""

    # member variables
    tableCount = 0
    lineCount = 0
    insideTable = False
    tradingTable = list()

    def handle_starttag(self, tag, attrs):
        if (tag == "table"):
            self.tableCount += 1
            self.lineCount = 0
            if (self.tableCount == 2):
                self.insideTable = True
        if (tag == "tr") and self.insideTable and (self.lineCount > 0):
            self.tradingTable.append(list())

    def handle_data(self, data):
        if self.insideTable and (self.lineCount > 0):
            self.tradingTable[-1].append(data.strip())

    def handle_endtag(self, tag):
        if (tag == "table"):
            self.insideTable = False
        if (tag == "tr") and self.insideTable:
            self.lineCount += 1

# parse HTML to extract table contents
parser = ASXHTMLParser()
parser.feed(html)

# extract equity trades by month
volume = []
months = []

for row in reversed(parser.tradingTable):
    months.append(row[0].replace(" ", "\n"))
    volume.append(float(row[1].replace(",", "")))

# define colour map
index = np.arange(len(volume));
cmap = matplotlib.cm.get_cmap('Spectral')
cnorm = matplotlib.colors.Normalize(0, len(index) - 1)
colours = cmap(cnorm(index))

# plot as bar chart (better)
plot_bar(volume, months, colours)

# plot as pie chart (perhaps not as helpful for this data!)
plot_pie(volume, months, colours)
