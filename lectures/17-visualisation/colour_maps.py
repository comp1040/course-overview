#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example colour maps.
#

import numpy as np
import matplotlib.pyplot as plt
import matplotlib

def show_colour_map(name):
    cmap = matplotlib.cm.get_cmap(name)
    patch = np.outer(np.ones(16), np.arange(0.0, 1.0, 1.0 / 255.0)) 
    plt.imshow(patch, cmap=cmap, aspect="auto", origin="lower")
    plt.ylabel(name, fontsize=16); plt.xticks([]); plt.yticks([])


# create ANU colour map
start_colour = "#af1e2d"  # red
end_colour = "#94b0bc"    # grey
cmap = matplotlib.colors.LinearSegmentedColormap.from_list("ANU", [start_colour, end_colour])
matplotlib.cm.register_cmap(name="ANU", cmap=cmap)

# show colour maps
names = ["jet", "hot", "cool", "Spectral", "ANU"]

plt.figure()
for i, n in enumerate(names):
    plt.subplot(len(names), 1, i + 1)
    show_colour_map(n)

plt.savefig("colourmaps.png", dpi=150, facecolor="white")
plt.show()
