#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example plotting to accompany Lecture 20: Visualisation.
#

import numpy as np
import matplotlib.pyplot as plt

# single plot default formatting

x = np.linspace(-5.0, 5.0, num=21, endpoint=True)
y = x ** 2 - 3.0 * x + 2.0
plt.plot(x, y)
plt.show()

# single plot marker formatting

plt.plot(x, y, 'bo')
plt.show()

# single plot user formatting

plt.plot(x, y, lw=2)
plt.xlabel('$x$', fontsize=16); plt.ylabel('$y = x^2 - 3x + 2$', fontsize=16)
plt.title('Example Parabola', fontsize=20)
plt.xlim([-5.0, 5.0])
plt.grid()
plt.show()

# multiple plots default formatting

y2 = x ** 2 - 1.5 * x - 1.0
plt.plot(x, y, lw=2)
plt.plot(x, y2, lw=2)   # alternatively, plt.plot(x, y, x, y2)
plt.xlabel('$x$', fontsize=16); plt.ylabel('$y$', fontsize=16)
plt.title('Example Parabolas', fontsize=20)
plt.xlim([-5.0, 5.0])
plt.grid()
plt.legend(['$y = x^2 - 3x + 2$', '$y = x^2 - 1.5x - 1$'], fontsize=20)
plt.show()

# log scale
plt.plot(x, y + 1.0)
plt.gca().set_yscale('log')
plt.xlim([-5.0, 5.0])
plt.grid()
plt.show()

# multiple subplots

plt.subplot(1, 2, 1)
plt.plot(x, y, lw=2)
plt.xlabel('$x$', fontsize=16); plt.ylabel('$y$', fontsize=16)
plt.title('$y = x^2 - 3x + 2$', fontsize=20)
plt.xlim([-5.0, 5.0])
plt.grid()

plt.subplot(1, 2, 2)
plt.plot(x, y2, 'g', lw=2)
plt.xlabel('$x$', fontsize=16)
plt.title('$y = x^2 - 1.5x - 1$', fontsize=20)
plt.xlim([-5.0, 5.0])
plt.grid()

plt.show()

# multiple figures

plt.figure()
plt.plot(x, y, lw=2)
plt.xlabel('$x$', fontsize=16); plt.ylabel('$y$', fontsize=16)
plt.title('$y = x^2 - 3x + 2$', fontsize=20)
plt.xlim([-5.0, 5.0])
plt.grid()

plt.figure()

plt.plot(x, y2, 'g', lw=2)
plt.xlabel('$x$', fontsize=16)
plt.title('$y = x^2 - 1.5x - 1$', fontsize=20)
plt.xlim([-5.0, 5.0])
plt.grid()

plt.show()
