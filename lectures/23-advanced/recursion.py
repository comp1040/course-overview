#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Examples of recursive functions.
#

# --- fibonacci sequence -------------------------------------------

def fibonacci_recusive(n):
    """Compute the n-th number in the Fibonacci sequence recursively."""

    # base cases
    if n == 0: return 0
    if n == 1: return 1

    # recursion
    return fibonacci_recusive(n - 1) + fibonacci_recusive(n - 2)

def fibonacci_iterative(n):
    """Compute the n-th number in the Fibonacci sequence iteratively."""

    # initialization
    f_last = 0
    f_curr = 1

    # iteration
    for i in range(1, n):
        f_curr, f_last = f_curr + f_last, f_curr

    return f_curr

# evaluate both functions on n == 10
print(fibonacci_recusive(10))
print(fibonacci_iterative(10))


