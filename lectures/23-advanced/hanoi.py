#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Recursive solution to the Towers of Hanoi problem.
#

class Hanoi(object):

    def __init__(self, n):
        """Initialise the board state with n disks."""
        self.n = n
        self.stacks = {'A': list(range(n, 0, -1)), 'B': [], 'C': []}

    def print_disk(self, m):
        """Prints the m-th disk."""
        prefix = ' ' * (self.n - m)
        print(prefix + ('*' * (2 * m + 1)) + prefix, end='')

    def print(self):
        """Prints the puzzle state."""
        empty_row = (' ' * self.n) + '|' + (' ' * self.n)
        for row in range(self.n, 0, -1):
            print('{0:2d}: '.format(row), end='')

            for col in ['A', 'B', 'C']:
                if len(self.stacks[col]) < row:
                    print(empty_row, end='')
                else:
                    self.print_disk(self.stacks[col][row - 1])
            print()
        print('    ' + '-' * (self.n * 6 + 3))
        print()

    def other(self, src, dst):
        """Returns the stack which is not `src` or `dst`."""
        return [disk for disk in self.stacks.keys() if disk not in [src, dst]][0]

    def move(self, src, dst):
        """Move the disk at the top of stack `src` to `dst`."""
        self.stacks[dst].append(self.stacks[src].pop())
        self.print()

    def move_partial_stack(self, m, src, dst):
        """Move stack of depth `m` from `src` to `dst`."""

        # base case
        if (m == 1):
            self.move(src, dst)
            return

        # recursion
        oth = self.other(src, dst)
        # 1. move m-1 disks from src to oth
        self.move_partial_stack(m - 1, src, oth)
        # 2. move m-th disk to dst
        self.move(src, dst)
        # 3. move m-1 disks from oth to src
        self.move_partial_stack(m - 1, oth, dst)

    def solve(self):
        """Solve the Towers of Hanoi problem."""
        self.move_partial_stack(self.n, 'A', 'C')


# --- Example puzzle ---------------------------------------------------

puzzle = Hanoi(4)   # construct the puzzle instance
puzzle.print()      # print the initial puzzle state
puzzle.solve()      # solve the puzzle



