#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example lambda function for sorting.
#

final_grades = [
    ("Homer Simpson", 33.75),
    ("Marge Simpson", 72.5),
    ("Bart Simpson", 50.0),
    ("Lisa Simpson", 98.5),
    ("Maggie Simpson", 8.75),
    ("Carl Carlson", 68.75),
    ("Ned Flanders", 72.5),
    ("Barney Gumble", 37.5),
    ("Lenny Leonard", 40.0),
    ("Otto Mann", 85.0),
    ("Seymour Skinner", 81.25)
]

sorted_by_name = sorted(final_grades)
for student in sorted_by_name:
    print(student)

sorted_by_grade = sorted(final_grades, key=lambda student: student[1])
for student in sorted_by_grade:
    print(student)

sorted_by_lastname = sorted(final_grades, key=lambda student: ''.join(student[0].split()[::-1]))
for student in sorted_by_lastname:
    print(student)
