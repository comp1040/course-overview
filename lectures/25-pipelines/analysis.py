#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Lecture 25: Data Pipelines and the Command Line
#
# Reads in a similarity file and plots a histogram of similarities.

import sys, getopt
import logging
import pickle
import matplotlib.pyplot as plt

def run(sims_file):
    """Read all similarities from given file and plot histogram."""
    logging.info('Building histogram for similarities in {}'.format(sims_file))
    with open(sims_file, 'rb') as file:
        allsims = pickle.load(file)

    values = []
    for doc, sims in allsims.items():
        values += sims.values()

    plt.hist(values)
    plt.show()

# -- main ---------------------------------------------------------------------

def usage(result=1):
    """Print usage statement."""
    print("USAGE: python3 " + sys.argv[0] + " <SIMS_FILE>")
    print("Plots a histogram of pairwise similarities.")
    sys.exit(result)

def main():
    # Make sure the SIMS_FILE is present
    if len(sys.argv) != 2:
        usage()

    # Run the pipeline stage
    run(sys.argv[1])


if __name__ == "__main__":
    main()
