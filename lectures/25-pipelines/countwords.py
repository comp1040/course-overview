#!/usr/bin/env python3
# COMP1040 Craft of Computing
#
# Counts the number of words (i.e., whitespace delimited strings)
# in a given file
#
# USAGE: countwords.py FILENAME
# AUTHOR: Mark Reid <mark.reid@anu.edu.au>

import sys

# Get the first command line argument after the script name
# as file to process
filename = sys.argv[1]

# Read in each line, split on white space, and count words
wordcount = 0
with open(filename) as file:
    for line in file:
        # Break up line into words and add to count
        wordcount += len(line.split())

# Display the count
print(wordcount)
