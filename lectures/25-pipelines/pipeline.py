#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Lecture 25: Data Pipelines and the Command Line
#
# Master file that defines data locations and coordinates all steps in the
# pipeline where each stage is a module. Each stage can also be run from
# the command line.

import os, logging
import fetch, extract, compare, analysis, graph

# Set level to logging.INFO or logging.DEBUG to see progress
logging.basicConfig(level=logging.DEBUG)

# Locations of data and storage
BASE_URL     = 'http://mark.reid.name/blog/past.html'
DATA_DIR     = 'data'
LINKS_FILE   = os.path.join(DATA_DIR, 'links.txt')
POSTS_DIR    = os.path.join(DATA_DIR, 'posts')
FEATURES_DIR = os.path.join(DATA_DIR, 'processed')
SIMS_FILE    = os.path.join(DATA_DIR, 'sims.pkl')

# Create the data directory (to reset simply remove this directory)
if not os.path.isdir(DATA_DIR):
    os.mkdir(DATA_DIR)

# Execute the pipeline
fetch.run(LINKS_FILE, POSTS_DIR, BASE_URL)
extract.run(POSTS_DIR, FEATURES_DIR)
compare.run(FEATURES_DIR, SIMS_FILE)
analysis.run(SIMS_FILE)
graph.run(SIMS_FILE, 0.75)

