#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Lecture 25: Data Pipelines and the Command Line
#
# Processes HTML files and converts them into a stemmed, tokenised,
# "bag of words" representation.

import os, sys, getopt
import pickle
import logging

from bs4 import BeautifulSoup
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer

# Ensure that the NLTK 'punkt' word list is downloaded so tokenizing works
import nltk
nltk.download('punkt')

# The CSS ID where the body text is located
TEXT_ID = 'page'
# Set up the stemmer to be used on the text
STEMMER = PorterStemmer()

def run(posts_dir, text_dir):
    """Reads in HTML files in posts_dir, extracts the text then
       stems and tokenizes."""

    logging.info('Extracting features from files in {}'.format(posts_dir))

    for filename in os.listdir(posts_dir):
        postpath = os.path.join(posts_dir, filename)
        logging.debug('Processing {} ...'.format(postpath))

        # Skip if processed file already exists
        filebase, fileext = os.path.splitext(filename)
        countpath = os.path.join(text_dir, filebase + '.bow')
        if os.path.exists(countpath):
            logging.debug('\tskipped ({} exists).'.format(countpath))
        else:
            # Ensure that text dir exists
            if not os.path.exists(text_dir):
                os.mkdir(text_dir)

            # Convert to counts and pickle
            counts = process(postpath)
            with open(countpath, 'wb') as countfile:
                pickle.dump(counts, countfile)
            logging.debug('\tsaved to {}'.format(countpath))

    logging.info('Extracting done!')

# -- helper functions ---------------------------------------------------------

def process(filename):
    """Extracts text from the HTML file then stems, tokenises & counts."""
    bodytext = get_bodytext(filename, TEXT_ID)
    tokens   = word_tokenize(bodytext)
    filtered = filter(lambda s: s.isalnum(), tokens)
    lower    = map(lambda s: s.lower(), filtered)
    stemmed  = map(lambda word: STEMMER.stem(word), lower)

    counts = {}
    for word in stemmed:
        if word in counts:
            counts[word] += 1
        else:
            counts[word] = 1

    return counts

def get_bodytext(filename, element_id):
    """Returns the text for the element with the given id in the HTML file."""
    with open(filename, 'rt', encoding='utf-8') as htmlfile:
        html = BeautifulSoup(htmlfile, 'html.parser')
        return ' '.join(html.find(id=element_id).strings)

# -- main ---------------------------------------------------------------------

def usage(result=1):
    """Print usage statement."""
    print("USAGE: python3 " + sys.argv[0] + " [<OPTIONS>] <POSTS_DIR> <TEXT_DIR>")
    print("Reads HTML files from <POSTS_DIR>, extracts the text then stems and\n"
          "tokenizes. The result for each HTML file is written to <TEXT_DIR>.")
    print("OPTIONS:")
    print("  --log=<level>  :: change logging level")
    print("  -h | --help    :: print help")
    sys.exit(result)

def main():
    # Parse command line arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["log=", "help"])
    except getopt.GetoptError:
        usage()

    # Make sure the POSTS_DIR and TEXT_DIR are present
    if len(args) != 2:
        usage()

    # Process the command line options
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("--log"):
            logging.basicConfig(level=getattr(logging, arg.upper(), None))

    # Run the pipeline stage
    run(args[0], args[1])


if __name__ == "__main__":
    main()
