#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Lecture 25: Data Pipelines and the Command Line
#
# Master file that defines data locations and coordinates all steps in the
# pipeline where each stage is a command line script. See also "pipeline.py"
# which treats each stage as a Python module.

import os
from subprocess import call

# Locations of data and storage
BASE_URL     = 'http://mark.reid.name/blog/past.html'
DATA_DIR     = 'data'
LINKS_FILE   = os.path.join(DATA_DIR, 'links.txt')
POSTS_DIR    = os.path.join(DATA_DIR, 'posts')
FEATURES_DIR = os.path.join(DATA_DIR, 'processed')
SIMS_FILE    = os.path.join(DATA_DIR, 'sims.pkl')
LOG_LEVEL    = 'DEBUG'

# Create the data directory (to reset simply remove this directory)
if not os.path.isdir(DATA_DIR):
    os.mkdir(DATA_DIR)

# Execute the pipeline as command line scripts
call(['python3', 'fetch.py', '--log=' + LOG_LEVEL, '-b', BASE_URL, LINKS_FILE, POSTS_DIR])
call(['python3', 'extract.py', '--log=' + LOG_LEVEL, POSTS_DIR, FEATURES_DIR])
call(['python3', 'compare.py', '--log=' + LOG_LEVEL, FEATURES_DIR, SIMS_FILE])
call(['python3', 'analysis.py', SIMS_FILE])
call(['python3', 'graph.py', '-t', str(0.75), SIMS_FILE])
