#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Lecture 25: Data Pipelines and the Command Line
#
# Reads in the similarities dictionary from a file, thresholds the
# similarities so that only larger ones are kept then display them as
# a graph

import sys, getopt
import pickle
import networkx as nx
import matplotlib.pyplot as plt
import logging

def run(sims_file, threshold):
    """Read in similarities, apply threshold, plot result as graph."""

    logging.info('Rendering {} as graph with threshold = {}'.format(sims_file, threshold))

    with open(sims_file, 'rb') as file:
        allsims = pickle.load(file)

    sims = {}
    for node, weights in allsims.items():
        sims[node] = dict((k, v) for k, v in weights.items() if v >= threshold)

    graph = nx.Graph(sims)
    nx.draw(graph)
    plt.show()

# -- main ---------------------------------------------------------------------

def usage(result=1):
    """Print usage statement."""
    print("USAGE: python3 " + sys.argv[0] + " [<OPTIONS>] <SIMS_FILE>")
    print("Visualises similarities as a graph. The -t option sets a threshold\n"
          "on which edges get displayed.")
    print("OPTIONS:")
    print("  -t <threshold> :: similarity threshold (default: 0.75)")
    print("  --log=<level>  :: change logging level")
    print("  -h | --help    :: print help")
    sys.exit(result)

def main():
    # Parse command line arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "t:h", ["threshold=", "log=", "help"])
    except getopt.GetoptError:
        usage()

    # Make sure the SIMS_FILE is present
    if len(args) != 1:
        usage()

    SIMS_FILE = args[0]
    THRESHOLD = 0.75

    # Process the command line options
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-t", "--threshold"):
            THRESHOLD = float(arg)
        elif opt in ("--log"):
            logging.basicConfig(level=getattr(logging, arg.upper(), None))

    # Run the pipeline stage
    run(SIMS_FILE, THRESHOLD)


if __name__ == "__main__":
    main()

