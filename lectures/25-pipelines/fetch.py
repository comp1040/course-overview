#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Lecture 25: Data Pipelines and the Command Line
#
# This script downloads blog posts from a links in a URL, e.g.,
#   "http://mark.reid.name/blog/past.html"

import os, sys, getopt
import logging

from urllib.request import urlopen, urlretrieve
from urllib.parse import urlsplit, urljoin
from bs4 import BeautifulSoup

def run(links_file, posts_dir, base_url = None):
    """Fetches posts from a given links file. If a base_url is provided
    then the base_url is parsed first to extract links and these are
    saved in the links file. Contents of links are downloaded to posts_dir."""

    # Extract links from base_url if provided.
    if base_url is not None:
        logging.info('Extracting links from {}'.format(base_url))
        links = find_links(base_url)
        logging.info('Saving links to {}'.format(links_file))
        with open(links_file, 'w') as f:
            for link in links:
                f.write(link + '\n')

    # If the posts directory does not exists, create and download posts
    if not os.path.isdir(posts_dir):
        logging.warning('No posts directory found at {}. Creating.'.format(posts_dir))
        os.mkdir(posts_dir)

    # Read through links and download each post, if necessary.
    with open(links_file, 'r') as f:
        for link in f:
            logging.debug('Fetching {}...'.format(link.strip()))
            post_file = post_filename(link.strip(), posts_dir)
            if os.path.exists(post_file):
                logging.debug('\tskipping ({} exists)'.format(post_file))
            else:
                urlretrieve(link.strip(), post_file)
                logging.debug('\tsaved to {}'.format(post_file))

    logging.info('Fetch complete. Posts in {}.'.format(posts_dir))

# -- helper functions ---------------------------------------------------------

def is_valid_link(link):
    """Is this a link we should keep?"""
    return link.startswith('../blog') and link.endswith('.html')

def find_links(url):
    """Extract links of the form 'blog/*.html' and return as list."""
    html = urlopen(url).read()
    doc = BeautifulSoup(html, 'html.parser')
    links = []
    for element in doc.find_all('a'):
        link = element.get('href')
        if is_valid_link(link):
            links.append(urljoin(url, link))
    return links

def post_filename(url, dir):
    """Construct a filename relative to directory for given URL."""
    name = os.path.split(urlsplit(url).path)[-1]
    return os.path.join(dir, name)

# -- main ---------------------------------------------------------------------

def usage(result=1):
    """Print usage statement."""
    print("USAGE: python3 " + sys.argv[0] + " [<OPTIONS>] <LINKS_FILE> <POSTS_DIR>")
    print("Fetches blog posts from links found in <LINKS_FILE> and stores them in\n"
          "the <POSTS_DIR> directory. The -b option updates the links file.")
    print("OPTIONS:")
    print("  -b <url>       :: base URL to update links file")
    print("  --log=<level>  :: change logging level")
    print("  -h | --help    :: print help")
    sys.exit(result)

def main():
    # Parse command line arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "b:h", ["base=", "log=", "help"])
    except getopt.GetoptError:
        usage()

    # Make sure the LINKS_FILE and POSTS_DIR are present
    if len(args) != 2:
        usage()

    LINKS_FILE = args[0]
    POSTS_DIR = args[1]

    # Process the command line options
    BASE_URL = None
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("-b", "--base"):
            BASE_URL = arg
        elif opt in ("--log"):
            logging.basicConfig(level=getattr(logging, arg.upper(), None))

    # Run the pipeline stage
    run(LINKS_FILE, POSTS_DIR, BASE_URL)


if __name__ == "__main__":
    main()
