#!/usr/bin/env python3
# COMP1040 The Craft of Computing
# Lecture 25: Data Pipelines and the Command Line
#
# Reads in the word count dictionaries from the given directory and
# computes a matrix of post similarities using the cosine distance
# and writes this out to a file.

import os, sys, getopt
import logging
import pickle
import math

def run(features_dir, out_file):
    """Compute the pairwise similarities of all bag-of-words files in dir.
       Write result out as { post: {post: similarity, ... } }."""

    logging.info('Comparing word frequencies in {}'.format(features_dir))

    # Do not recompute if file exists
    if os.path.exists(out_file):
        logging.warning('Similarities file {} already exists... skipping'.format(out_file))
        return

    # Read in all the features and names
    features = []
    for filename in os.listdir(features_dir):
        name, ext = os.path.splitext(filename)
        filepath = os.path.join(features_dir, filename)
        with open(filepath, 'rb') as file:
            bow = pickle.load(file)
            features.append((name, bow))
    logging.info('All word frequency dictionaries loaded')

    # Build the similarity dictionary
    logging.info('Comparing dictionaries...')
    similarities = {}
    while features:
        name1, doc1 = features.pop()
        logging.debug('\tFor {}...'.format(name1))
        sims = {}
        for name2, doc2 in features:
            sims[name2] = similarity(doc1, doc2)
        similarities[name1] = sims

    # Save the results
    logging.info('Saving similarities to {}'.format(out_file))
    pickle.dump(similarities, open(out_file, 'wb'))

    logging.info('Comparison complete.')


# -- helper functions ---------------------------------------------------------

def similarity(d1, d2):
    """Computes the cosine similarity between two documents."""
    return inner(d1, d2) / (norm(d1) * norm(d2))

def inner(d1, d2):
    """Computes the inner product between two dictionaries of word counts."""
    # Can skip over any zero count words in either dictionary since they
    # will not add to total
    sumprod = 0
    for word, count1 in d1.items():
        if word in d2:
            sumprod += count1 * d2[word]
    return sumprod

def norm(d):
    """Return the Euclidean norm of the word count vector in d."""
    return math.sqrt(inner(d,d))

# -- main ---------------------------------------------------------------------

def usage(result=1):
    """Print usage statement."""
    print("USAGE: python3 " + sys.argv[0] + " [<OPTIONS>] <FEATURES_DIR> <OUT_FILE>")
    print("Computes the pairwise similarity as a bag-of-words model\n"
          "for all files in <FEATURES_DIR>.")
    print("OPTIONS:")
    print("  --log=<level>  :: change logging level")
    print("  -h | --help    :: print help")
    sys.exit(result)

def main():
    # Parse command line arguments
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h", ["log=", "help"])
    except getopt.GetoptError:
        usage()

    # Make sure the FEATURES_DIR and OUT_FILE are present
    if len(args) != 2:
        usage()
    
    # Process the command line options
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage(0)
        elif opt in ("--log"):
            logging.basicConfig(level=getattr(logging, arg.upper(), None))

    # Run the pipeline stage
    run(args[0], args[1])


if __name__ == "__main__":
    main()
