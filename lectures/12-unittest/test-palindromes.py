# COMP1040 The Craft of Computing
#
# An example of unit testing for a palindrome checking and creation.

def ispalindrome(s):
    """Returns True if and only if the string s is the same
    when it is reversed (ignoring case)."""
    # TODO: Implement a palindrome checker
    return s[::-1].lower() == s.lower()

import unittest
class PalindromeTest(unittest.TestCase):

    def test_ispalindrome(self):
        self.assertTrue(ispalindrome("mum"), msg="mum is a palindrome")
        self.assertTrue(ispalindrome("Glenelg"))
        self.assertFalse(ispalindrome("dog"))


