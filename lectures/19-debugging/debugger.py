#!/usr/bin/env python3
# COMP1040: The Craft of Computing
# Lecture 19: Debugging
#
# NOTE: This code is deliberately incorrect! Use the PyCharm 
#       debugger to walk through this code to find and fix the 
#       bugs. There are at least two.
#
# The intention of the code is to take as input a dictionary
# containing lists of numbers as values and producing the sum 
# of those values for each dictionary entry.

def summarise(results):
    """Summarise a dictionary by calculating the total of the 
       list of numbers associated with each key."""

    for key, values in results.items():
        results = { key: total(values) }

    return results

def total(xs):
    """Computes the total of the numbers in the given list."""
    result = 0
    for x in xs:
        result += result + x

    return result

# Simple example usage
simple = { 'a': [1, 2] }
print(summarise(simple))

# More complex example usage
sales = { 'apples': [89, 55, 20], 'oranges': [30, 67, 90]}
print(summarise(sales))
