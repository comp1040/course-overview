#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example of finding a name in a guest list.

# --- create and sort guest list ------------------------------------------

guest_list = [
    "Barak Obama",
    "Justin Bieber",
    "Marylyn Monroe",
    "Bob Dylan",
    "Tom Cruise",
    "Angelina Jolie",
    "Oprah Winfrey",
    "Taylor Swift",
    "Justin Timberlake",
    "Elvis Presley",
    "Britney Spears",
    "Katy Perry",
    "Miley Cyrus",
    "Jennifer Garner",
    "David Beckham",
    "Brad Pitt",
    "Ben Affleck",
    "Audrey Hepburn",
    "Rihanna",
    "Robin Williams"
]

guest_list.sort()
print("{} guests".format(len(guest_list)))

# --- get query name ------------------------------------------------------

query = input("Query name: ")

# --- find name by brute force search -------------------------------------

name_found = False
for name in guest_list:
    if (name == query):
        name_found = True
        break

if name_found:
    print("{} is a guest".format(query))
else:
    print("{} is not on the guest list".format(query))

# --- find name by binary search ------------------------------------------

lower_bound = 0
upper_bound = len(guest_list) - 1
while (upper_bound > lower_bound):
    middle_index = int((lower_bound + upper_bound) / 2)
    if (query > guest_list[middle_index]):
        lower_bound = middle_index + 1
    else:
        upper_bound = middle_index

name_found = (query == guest_list[lower_bound])
if name_found:
    print("{} is a guest".format(query))
else:
    print("{} is not on the guest list".format(query))
