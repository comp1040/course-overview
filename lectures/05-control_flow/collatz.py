#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example code for computing a Collatz sequence starting from
# user provided input.

n = int(input('Enter a positive integer:'))

# check that the input was positive
if n <= 0:
    exit()

# keep generating the next integer in the sequence until we reach n == 1
print(n)
while (n != 1):
    if (n % 2 == 0):
        n = int(n / 2)
    else:
        n = 3 * n + 1
    print(n)
