#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# A substitution cipher is one of the earliest forms of cryptography
# in which a plaintext message is converted to ciphertext (i.e.,
# encrypted) by substituting letters via a lookup table. The
# ciphertext is decrypted by performing the reverse substitution.
#

import random

def encrypt_or_decrypt(input_message, key):
    """Encrypts or decrypts a message given a key."""
    output_message = ''
    for ch in input_message.upper():
        if str.isalpha(ch):
            output_message += chr(ord('A') + key[ord(ch) - ord('A')])
        else:
            output_message += ch

    return output_message

# generate a random cipher (encryption key)
encryption_key = list(range(26))
random.shuffle(encryption_key)

# create the reverse cipher (decryption key)
decryption_key = list(range(26))
for i in range(26):
    decryption_key[encryption_key[i]] = i

# encode a simple message
message = "Hello World!!!"
print(message)

coded_message = encrypt_or_decrypt(message, encryption_key)
print(coded_message)

decoded_message = encrypt_or_decrypt(coded_message, decryption_key)
print(decoded_message)
