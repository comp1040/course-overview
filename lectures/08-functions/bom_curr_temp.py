#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# Example of extracting current temperatures of capital cities
# from the BOM website.
#

import urllib.request
from html.parser import HTMLParser

# query the BOM main page using urllib
URL = "http://www.bom.gov.au"

response = urllib.request.urlopen(URL)
html = str(response.read())
response.close()

# class to parse the website derived from HTMLParser
class BOMHTMLParser(HTMLParser):
    """Parser to extract capital city temperature from the
    main BOM webpage."""

    # member variables
    insideH3Tag = False
    insideNowTemp = False
    lastCityName = ""
    lastTemperature = ""

    def handle_starttag(self, tag, attrs):
        if (tag == "h3"):
            self.insideH3Tag = True
        if (tag == "p") and (len(attrs) > 0) and (attrs[0][1] == "now"):
            self.insideNowTemp = True

    def handle_data(self, data):
        if self.insideH3Tag:
            self.lastCityName = data
        if self.insideNowTemp:
            self.lastTemperature = data

    def handle_endtag(self, tag):
        if (tag == "h3"):
            self.insideH3Tag = False
        if (tag == "p") and self.insideNowTemp:
            self.insideNowTemp = False
            print("{}\t{}".format(self.lastTemperature, self.lastCityName))


# parse HTML to extract temperatures
parser = BOMHTMLParser()
parser.feed(html)
