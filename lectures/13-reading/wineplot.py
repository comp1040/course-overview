# COMP1040 Craft of Computing
#
# Example code for reading in data from a CSV file then
# doing a 3D plot of some features.
#
# AUTHOR: Mark Reid

# Read in the data and get features
import csv
def get_features(xname, yname):
    """
    Get the features of the wine data by name (see header of datafile).
    The z-coordinate will contain the 'quality' feature.

    :param xname: The name of the x-coordinate feature
    :param yname: The name of the y-coordinate feature
    :return: Dictionary from keys 'x', 'y', 'z' to
             pairs of (name, [float]).
    """
    with open('winequality-red.csv') as datafile:
        xs, ys, zs, cols = [], [], [], []
        for row in csv.DictReader(datafile, delimiter=';'):
            xs.append(float(row[xname]))
            ys.append(float(row[yname]))
            zs.append(float(row['quality']))
    return { 'x': (xname, xs), 'y': (yname, ys), 'z': ('quality', zs) }

# Plot data in 3D scatter plot
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def plot_features_vs_quality(xname, yname):
    """
    Plot the two named features vs. wine quality.

    :param xname: The name of the first feature.
    :param yname: The name of the second feature.
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    data = get_features(xname, yname)
    colors = make_colors(data['z'][1], 6)
    ax.scatter(data['x'][1], data['y'][1], data['z'][1], c=colors)

    ax.set_xlabel(data['x'][0])
    ax.set_ylabel(data['y'][0])
    ax.set_zlabel('Quality')

    plt.show()

def make_colors(values, threshold):
    """
    Create a list of colors based on whether the values are above
    (red) or below (blue) the given threshold.
    Note quality values are between 1 and 10.

    :param threshold: The threshold to test against.
    :return: A list of colors ('r' or 'b')
    """
    cols = []
    for v in values:
        if v > threshold:
            cols.append('r')
        else:
            cols.append('b')
    return cols

#======================================================================
plot_features_vs_quality('pH', 'alcohol')