# Top 250 Movie Actors and Actresses

This repository contains a JSON file `acting.json` which maps from
actor names to the top 250 films they have appeared in.

The file was generated from the `imdb_top_250.csv` file downloaded
from [here][top250] and `actors.list` and `actress.list` files from
the [public datasets][imdb] that IMDB makes available via FTP 
(e.g., from <ftp://ftp.fu-berlin.de/pub/misc/movies/database/>).

The files were preprocessed by scanning through the 27 million lines
in `actors.list` and `actresses.list` to find all those actors actors
who have starred in one of the top 250 movies. Actors appearing in
less than two movies where then removed. Using a regular expression
for matching movie titles, the script takes around 4 minutes on a Xeon
3.4GHz processor.

[top250]: http://zegoggl.es/data/imdb_top_250.csv
[imdb]: http://www.imdb.com/interfaces