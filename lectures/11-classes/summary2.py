#!/usr/bin/env python3
# COMP1040 The Craft of Computing
#
# A second implementation of Summary with an emphasis on efficiency.

class Summary:
    """
    A summary incrementally keeps track of key statistics such as the
    number, mean, and variance of a collection of numbers.
    (This version requires constant space and time).
    """

    # A class variable that keeps track of the number summary instances
    num_summaries = 0

    def __init__(self):
        """Create an empty summary."""
        self.count = 0
        # Keep track of sum and sum of squares.
        self.sum         = 0.0
        self.sum_squares = 0.0

        Summary.num_summaries += 1
        self.id          = Summary.num_summaries

    def add(self, x):
        """
        Add x to this summary and update the statistics.
        :param x: The number to add.
        """
        self.count += 1
        self.sum   += x
        self.sum_squares += x**2

    def addAll(self, xs):
        """
        Add a list of numbers to this summary.
        :param xs: A list of numbers.
        """
        for x in xs:
            self.add(x)

    def statistics(self):
        """
        Returns the count, mean, and (biased) sample variance of the numbers
        added to this summary.
        :return: The triple (count, mean, variance).
        """
        n    = self.count
        mean = self.sum / float(n)
        var  = self.sum_squares / float(n) - mean**2
        return (n, mean, var)


# Test the summary class with some simple examples
s1 = Summary()
s1.addAll([1,2,3])
print("Summary #{}: {}".format(s1.id,s1.statistics()))
s1.addAll([1,1])
print("Summary #{}: {}".format(s1.id,s1.statistics()))

s2 = Summary()
s2.addAll([1,1,1])
print("Summary #{}: {}".format(s2.id,s2.statistics()))
